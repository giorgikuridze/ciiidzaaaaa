package com.example.group11lecture4

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_first.*

class ThirdActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)
        init()
    }

    private fun init() {
        logInButton.setOnClickListener {
            if (emailEditText.text.isNotEmpty() && passwordEditText.text.isNotEmpty()) {
                Toast.makeText(this, "Sign Up Success", Toast.LENGTH_LONG).show()
                val intent = Intent(this, FirstActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Sign Up Failed", Toast.LENGTH_LONG).show()
            }

        }
    }
}